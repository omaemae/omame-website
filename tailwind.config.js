module.exports = {
	content: ['./src/**/*.tsx'],
	theme: {
		extend: {
			fontFamily: {
				sans: ['InterVariable', 'Roboto', 'Arial', '-system-ui'],
			},
			colors: {
				omame: {
					primary: '#6c71b1',
					secondary: '#9499db',
					tertiary: '#2c2f56',
					evenDarker: '#181a38',
				},
			},
			backgroundImage: {
				'gradient-radial':
					'radial-gradient(circle max(75vh, 55vw) at bottom, var(--tw-gradient-stops))',
			},
		},
	},
}
