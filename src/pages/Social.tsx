import BackButton from '@/components/BackButton'
import SocialLink from '@/components/SocialLink'
import {
	CodeBracketIcon,
	GlobeAltIcon,
	PaintBrushIcon,
} from '@heroicons/react/24/solid'
import anime from 'animejs'
import { ComponentChildren } from 'preact'
import { useEffect } from 'preact/hooks'
import {
	siDiscord,
	siFuraffinity,
	siGithub,
	siGitlab,
	siInstagram,
	siMastodon,
	siTelegram,
	siTumblr,
	siTwitter,
} from 'simple-icons/icons'

const SocialSection = (props: { children?: ComponentChildren }) => (
	<div class="flex flex-col gap-2 rounded-xl bg-omame-evenDarker/40 p-6">
		{props.children}
	</div>
)

const Social = () => {
	useEffect(() => {
		const tl = anime.timeline()
		tl.add({
			targets: 'h1',
			opacity: [0, 1],
			translateY: ['-5rem', 0],
			easing: 'spring(1, 60, 10, 0)',
		})
		tl.add(
			{
				targets: '#sections > div',
				opacity: [0, 1],
				translateY: ['2rem', 0],
				easing: 'spring(2, 80, 10, 0)',
				delay: anime.stagger(200),
			},
			300
		)
	})

	const doFade = () =>
		new Promise<void>((resolve) => {
			anime({
				targets: ['#page'],
				opacity: 0,
				translateY: '2rem',
				duration: 400,
				easing: 'easeOutQuad',
				complete: () => {
					resolve()
				},
			})
		})

	return (
		<div
			id="page"
			className="flex min-h-screen flex-col items-center justify-center gap-6 px-6 py-12"
		>
			<div>
				<h1 class="flex items-center gap-6 text-4xl font-semibold opacity-0">
					<BackButton beforeNav={doFade} />
					social media
				</h1>
				<div id="sections" class="mt-8 flex flex-col gap-4 [&>div]:opacity-0">
					<SocialSection>
						<h2 class="mb-2 flex items-center gap-2 text-2xl font-semibold">
							<GlobeAltIcon className="h-6 w-6" />
							General
						</h2>
						<SocialLink
							platform={'Mastodon'}
							title={'@omame@woof.tech'}
							icon={siMastodon}
							link={'https://woof.tech/@omame'}
						/>
						<SocialLink
							platform={'Discord'}
							title={'omame#7818'}
							icon={siDiscord}
							link={'https://discord.gg/jXSanCxn77'}
						/>
						<SocialLink
							platform={'Telegram'}
							title={'@omametech'}
							icon={siTelegram}
							link={'https://t.me/omametech'}
						/>
					</SocialSection>
					<SocialSection>
						<h2 class="mb-2 flex items-center gap-2 text-2xl font-semibold">
							<PaintBrushIcon className="h-6 w-6" />
							Art
						</h2>
						<SocialLink
							platform={'Telegram'}
							title={'@omameart'}
							icon={siTelegram}
							link={'https://t.me/omameart'}
						/>
						<SocialLink
							platform={'Twitter'}
							title={'@omameart'}
							icon={siTwitter}
							link={'https://twitter.com/omameart'}
						/>
						<SocialLink
							platform={'Instagram'}
							title={'@omametech'}
							icon={siInstagram}
							link={'https://instagram.com/omametech/'}
						/>
						<SocialLink
							platform={'Tumblr'}
							title={'@omametech'}
							icon={siTumblr}
							link={'https://omametech.tumblr.com'}
						/>
						<SocialLink
							platform={'Fur Affinity'}
							title={'~omametech'}
							icon={siFuraffinity}
							link={'https://furaffinity.net/user/omametech'}
						/>
					</SocialSection>
					<SocialSection>
						<h2 class="mb-2 flex items-center gap-2 text-2xl font-semibold">
							<CodeBracketIcon className="h-6 w-6" />
							Code
						</h2>
						<SocialLink
							platform={'GitLab'}
							title={'@omaemae'}
							icon={siGitlab}
							link={'https://gitlab.com/omaemae'}
						/>
						<SocialLink
							platform={'GitHub'}
							title={'@omaemae'}
							icon={siGithub}
							link={'https://github.com/omaemae'}
						/>
					</SocialSection>
				</div>
			</div>
		</div>
	)
}

export default Social
