import BackButton from '@/components/BackButton'
import Project from '@/components/Project'
import { projects } from '@/assets/metadata.json'
import { useEffect } from 'preact/hooks'
import anime from 'animejs'

const Projects = () => {
	useEffect(() => {
		const tl = anime.timeline()
		tl.add({
			targets: 'h1',
			opacity: [0, 1],
			translateY: ['-5rem', 0],
			easing: 'spring(1, 60, 10, 0)',
		})
		tl.add(
			{
				targets: '#projects > div',
				opacity: [0, 1],
				translateY: ['2rem', 0],
				easing: 'spring(2, 80, 10, 0)',
				delay: anime.stagger(200),
			},
			300
		)
	})

	const doFade = () =>
		new Promise<void>((resolve) => {
			anime({
				targets: ['#page'],
				opacity: 0,
				translateY: '2rem',
				duration: 400,
				easing: 'easeOutQuad',
				complete: () => {
					resolve()
				},
			})
		})

	return (
		<div
			id="page"
			className="flex min-h-screen flex-col items-center justify-center gap-6 px-6 py-12"
		>
			<div>
				<h1 class="flex items-center gap-6 text-4xl font-semibold opacity-0">
					<BackButton beforeNav={doFade} />
					projects
				</h1>
				<div id="projects" class="mt-8 flex flex-col gap-4 [&>div]:opacity-0">
					{projects.map((project) => (
						<Project {...project} />
					))}
				</div>
			</div>
		</div>
	)
}

export default Projects
