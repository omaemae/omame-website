import NavButton from '@/components/NavButton'
import SIIcon from '@/components/SIIcon'
import {
	FolderOpenIcon,
	GlobeEuropeAfricaIcon,
} from '@heroicons/react/24/solid'
import anime from 'animejs'
import { useEffect } from 'preact/hooks'
import { siKofi } from 'simple-icons/icons'
import { commissions } from '@/assets/metadata.json'

const Index = () => {
	useEffect(() => {
		const tl = anime.timeline()
		tl.add({
			targets: '#index > h1',
			opacity: [0, 1],
			translateY: ['-5rem', 0],
			easing: 'spring(1, 60, 10, 0)',
		})
		tl.add(
			{
				targets: '#index > .flex > *',
				opacity: [0, 1],
				translateY: ['2rem', 0],
				easing: 'spring(1, 80, 10, 0)',
				delay: anime.stagger(75),
			},
			200
		)
		tl.add(
			{
				targets: '#index > .grid > a',
				opacity: [0, 1],
				translateY: ['5rem', 0],
				easing: 'spring(1, 40, 10, 0)',
				delay: anime.stagger(100),
			},
			400
		)
	})

	const doFade = () =>
		new Promise<void>((resolve) => {
			anime({
				targets: ['#index'],
				opacity: 0,
				translateY: '2rem',
				duration: 400,
				easing: 'easeOutQuad',
				complete: () => {
					resolve()
				},
			})
		})

	return (
		<div
			id="index"
			class="flex min-h-screen flex-col items-center justify-center gap-6 p-6"
		>
			<h1 class="text-7xl font-semibold opacity-0">omame</h1>
			<div class="flex items-center gap-4 text-xl [&>*]:opacity-0">
				<p>programmer</p>
				<div class="h-2 w-2 rounded-full bg-omame-secondary/40"></div>
				<p>designer</p>
				<div class="h-2 w-2 rounded-full bg-omame-secondary/40"></div>
				<p>artist</p>
			</div>
			<div class="grid gap-2 md:grid-rows-2 [&>a]:opacity-0">
				<NavButton
					icon={<FolderOpenIcon />}
					name="projects"
					target="/projects"
					beforeNav={doFade}
				/>
				<NavButton
					icon={<GlobeEuropeAfricaIcon />}
					name="social media"
					target="/social"
					beforeNav={doFade}
				/>
				<NavButton
					icon={<SIIcon icon={siKofi} />}
					name={commissions ? 'commissions open!' : 'buy me a ko-fi! <3'}
					target={
						'https://ko-fi.com/omametech' + (commissions ? '/commissions' : '')
					}
					simpleNav
					className="md:col-span-2"
				/>
			</div>
		</div>
	)
}

export default Index
