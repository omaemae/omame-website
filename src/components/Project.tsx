import SIIcon from '@/components/SIIcon'
import { GlobeAltIcon } from '@heroicons/react/20/solid'
import { siGithub } from 'simple-icons/icons'

const Project = (props: {
	title: string
	type: string
	description: string
	links?: {
		website?: string
		github?: string
	}
}) => (
	<div class="max-w-md rounded-xl bg-omame-evenDarker/40 p-6">
		<h2 class="text-2xl font-semibold">{props.title}</h2>
		<p class="text-omame-secondary">{props.type}</p>
		<p class="mt-4">{props.description}</p>
		{props.links && (
			<ul class="mt-4 flex flex-col gap-1">
				{props.links.website && (
					<li>
						<a
							href={props.links.website}
							class="flex items-center gap-2 font-semibold hover:underline"
						>
							<GlobeAltIcon className="h-5 w-5" />
							Website
						</a>
					</li>
				)}
				{props.links.github && (
					<li>
						<a
							href={props.links.github}
							class="flex items-center gap-2 font-semibold hover:underline"
						>
							<SIIcon icon={siGithub} className="h-5 w-5" />
							GitHub
						</a>
					</li>
				)}
			</ul>
		)}
	</div>
)

export default Project
