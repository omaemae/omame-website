import { SimpleIcon } from 'simple-icons'

const SIIcon = (props: { icon: SimpleIcon; className?: string }) => (
	<svg role="img" viewBox="0 0 24 24" className={props.className}>
		<path d={props.icon.path} fill="currentColor" />
	</svg>
)

export default SIIcon
