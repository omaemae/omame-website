import { SimpleIcon } from 'simple-icons'
import SIIcon from '@/components/SIIcon'

const fuckedColors = ['181717', '36566F', '36465D']

const SocialLink = (props: {
	platform: string
	title: string
	icon: SimpleIcon
	link: string
}) => (
	<a
		target={'_blank'}
		class={
			'flex items-center hover:underline [&_svg]:mr-2 [&_svg]:h-5 [&_svg]:w-5'
		}
		href={props.link}
	>
		<span
			style={`color: #${
				fuckedColors.indexOf(props.icon.hex) == -1 ? props.icon.hex : 'ffffff'
			};`}
		>
			<SIIcon icon={props.icon} />
		</span>
		<span
			style={`color: #${
				fuckedColors.indexOf(props.icon.hex) == -1 ? props.icon.hex : 'ffffff'
			};`}
			class={'font-semibold'}
		>
			{props.platform}
		</span>
		: {props.title}
	</a>
)

export default SocialLink
