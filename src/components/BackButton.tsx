import { ArrowLeftIcon } from '@heroicons/react/24/solid'
import { Link, useNavigate } from 'react-router-dom'

const BackButton = (props: { beforeNav?: () => Promise<void> }) => {
	const navigate = useNavigate()
	return (
		<Link
			to="/"
			className="rounded-full bg-omame-evenDarker/40 p-3 transition-all hover:bg-omame-secondary hover:text-omame-evenDarker"
			title="Back to Homepage"
			onClick={async (e: MouseEvent) => {
				e.preventDefault()
				if (props.beforeNav) await props.beforeNav()
				navigate('/')
			}}
		>
			<ArrowLeftIcon className="h-6 w-6" />
		</Link>
	)
}

export default BackButton
