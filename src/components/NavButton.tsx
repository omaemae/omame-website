import classNames from 'classnames'
import { ComponentChild } from 'preact'
import { useNavigate } from 'react-router-dom'

const NavButton = (props: {
	icon: ComponentChild
	name: string
	target: string
	beforeNav?: () => Promise<void>
	simpleNav?: boolean
	className?: string
}) => {
	const navigate = useNavigate()
	return (
		<a
			href={props.target}
			className={classNames(
				'flex justify-center gap-2 rounded-full bg-omame-evenDarker/40 px-6 py-4 font-semibold transition-colors [&>svg]:w-6',
				'hover:bg-omame-secondary hover:text-omame-evenDarker',
				props.className
			)}
			onClick={async (e) => {
				e.preventDefault()
				if (props.beforeNav) {
					await props.beforeNav()
				}
				if (!props.simpleNav) navigate(props.target)
				else location.href = props.target
			}}
		>
			{props.icon}
			{props.name}
		</a>
	)
}

export default NavButton
