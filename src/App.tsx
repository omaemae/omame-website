import '@fontsource/inter/variable.css'
import '@/styles.css'
import Index from '@/pages/Index'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Projects from '@/pages/Projects'
import Social from '@/pages/Social'

const App = () => (
	<BrowserRouter>
		<Routes>
			<Route path={'/'} element={<Index />} />
			<Route path={'projects'} element={<Projects />} />
			<Route path={'social'} element={<Social />} />
		</Routes>
	</BrowserRouter>
)

export default App
