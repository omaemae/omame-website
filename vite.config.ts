import path from 'path'
import { UserConfig } from 'vite'
import preact from '@preact/preset-vite'

const config: UserConfig = {
    plugins: [preact()],
	resolve: {
		alias: {
			'@': path.join(__dirname, 'src'),
		},
	},
}

export default config
